from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm, TodoUpdateForm, CreateForm, UpdateForm
# Create your views here.


def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": detail,
        "todo_items": detail,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoUpdateForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoUpdateForm()
        context = {
            "form": form,
        }
    return render(request, "todos/update.html", context)


def delete_todo_list(request, id):
    delete_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = CreateForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create_item.html", context)


def update_item(request, id):
    if request.method == "POST":
        form = UpdateForm(request.POST)
        if form.is_valid:
            update = form.save()
            return redirect("todo_list_detail", id=update.list.id)
    else:
        form = UpdateForm()
        context = {
            "form": form,
        }
    return render(request, "todos/update_item.html", context)
